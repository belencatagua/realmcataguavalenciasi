package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {

    @Required
    private String idestudiante;
    private String idmateria;
    private String calendario;
    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _idestudiante, String _idmateria, String _calendario)
    { this.idestudiante = _idestudiante; this.idmateria = _idmateria; this.calendario = _calendario; }
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }
    public String getIdestudiante() { return idestudiante; }
    public void setIdestudiante(String idestudiante) { this.idestudiante = idestudiante; }

    public String getIdmateria() {return idmateria;}
    public void  setIdmateria(String idmateria) {this.idmateria= idmateria;}

    public String getCalendario() {return calendario;}
    public void  setCalendario(String calendario) {this.calendario= calendario;}


}
